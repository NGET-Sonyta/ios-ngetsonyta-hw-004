
import UIKit

class NoteWriteViewController: UIViewController, UITextFieldDelegate,UINavigationControllerDelegate, UITextViewDelegate  {
   
    var newNotes: String = "NewNote".localizedString()
   
    // Reference to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var newNote: UILabel!
    
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var txtTitle: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtTitle.delegate = self
        txtDescription.delegate = self
        txtTitle.becomeFirstResponder()
        txtDescription.becomeFirstResponder()
        newNote.text = newNotes
    }
    
    // Execute once the ViewController disappears
    override func viewWillDisappear(_ animated: Bool) {
        
        
        guard let title = txtTitle.text else {return}
        guard let description = txtDescription.text else {return}

        if title == "" || description == "" {
            
            return
            
        }else if title != nil && description != nil{
            
            if title.count < 12 && description.count < 100 {
                let myNote = Note(context: self.context)
                myNote.myTitle = title
                myNote.myDescription = description
                try? self.context.save()
                
            }
        }
    }
}
