import UIKit


protocol UpdateNote{
    func backToNote(titleEdit: String, descriptionEdit: String, indexNote: Int)
    
}

class NoteEditViewController: UIViewController, EditNote {
   
    func leadToNote(titleEdit: String, descriptionEdit: String, indexNote: Int) {
        mylabel = titleEdit
        myDes = descriptionEdit
        noteIndex = indexNote
        print("The index")
        print(indexNote)
    }
    
    
    var noteIndex: Int?
    var delegateUpdate: UpdateNote?
    
    var edit: String = "Edit".localizedString()
    var cancel: String = "Cancel".localizedString()
    var checkbox: String = "Checkbox".localizedString()
    var Recording: String = "Recording".localizedString()
    var Drawing: String = "Drawing".localizedString()
    var chooseImage: String = "ChooseImage".localizedString()
    var takePhoto: String = "TakePhoto".localizedString()
    var delete: String = "Delete".localizedString()
    var copy: String = "Copy".localizedString()
    var send: String = "Send".localizedString()
    var collaborators: String = "Collaborators".localizedString()
    var labels: String = "Label".localizedString()
    
    // Reference to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    
    @IBOutlet weak var txtEdit: UILabel!
    @IBOutlet weak var txtTitle: UITextView!
    @IBOutlet weak var txtDescription: UITextView!
    
    var mylabel = ""
    var myDes = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        txtTitle.text = mylabel
        txtDescription.text = myDes
        txtTitle.isEditable = true
        txtDescription.isEditable = true
    
    }
    
   
        // Execute once the ViewController disappears
        override func viewWillDisappear(_ animated: Bool) {
            guard let title = txtTitle.text else {return}
            guard let description = txtDescription.text else {return}

            if title == "" || description == "" {
                
                return
                
            }else if title != nil && description != nil{
                
                if title.count < 12 && description.count < 100 {
              
                    let a = txtTitle.text ?? ""
                    let b = txtDescription.text ?? ""
            
                    var c = noteIndex!

                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "viewController") as! ViewController
                            
                    self.delegateUpdate = viewController as UpdateNote

                    delegateUpdate?.backToNote(titleEdit: a, descriptionEdit: b, indexNote: c)
                }
    }
}
    
    @IBAction func barBtnItemLeft(_ sender: Any) {
        
        // Create alert
        let alert = UIAlertController(title: nil, message: nil , preferredStyle: UIAlertController.Style.actionSheet)
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: takePhoto, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: chooseImage, style: .default, handler: { _ in
            }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: Drawing, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: Recording, style: .default, handler: { _ in
            }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: checkbox, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: { _ in
                }
        ))
        
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func barBtnItemRight(_ sender: Any) {
        
        // Create alert
        let alert = UIAlertController(title: nil, message: nil , preferredStyle: UIAlertController.Style.actionSheet)
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: delete, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: copy, style: .default, handler: { _ in
            }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: send, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: collaborators, style: .default, handler: { _ in
            }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: labels, style: .default, handler: { _ in
                }
        ))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: { _ in
                }
        ))

        self.present(alert, animated: true, completion: nil)
        
    }
}
