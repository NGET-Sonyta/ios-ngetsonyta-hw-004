import UIKit
import CoreData

protocol EditNote{
    func leadToNote(titleEdit: String, descriptionEdit: String, indexNote: Int)
}

class customizedLongPress:UILongPressGestureRecognizer{
     var indexCell: Int?
}

class ViewController: UIViewController, UpdateNote {
    
    var titleAlert: String = "Language".localizedString()
    var descriptionAlert: String = "DescriptionAlert".localizedString()
    var khmerAlert: String = "Khmer".localizedString()
    var englishAlert: String = "English".localizedString()
    var cancelAlert: String = "Cancel".localizedString()
    var takeNote: String = "TakeNote".localizedString()
    var deleteDescription: String = "DeleteDescription".localizedString()
    var delete: String = "Delete".localizedString()
    

    @IBOutlet weak var barBtnTakeNote: UIBarButtonItem!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    var indexPathEdit: Int?
    // Data for the CollectionView
    var myNotes:[Note] = []
    
    

    var getTitle: String = ""
    var getDescription: String = ""
    
    // Referencr to managed object context
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var delegate: EditNote?
    
    func fetchNote(){
        
        //Fetch the data from Core Data to display in collectionViewCell
        do{
            self.myNotes = try context.fetch(Note.fetchRequest())
            
            DispatchQueue.main.async {
              self.collectionView.reloadData()
            }
        }catch{
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        // Get items from Core Data
        fetchNote()
    }
    
    
    @objc func delete1(sender: customizedLongPress){
        
        // Create alert
        let alert = UIAlertController(title: nil, message: deleteDescription , preferredStyle: UIAlertController.Style.actionSheet)
        
        if sender.state == UIGestureRecognizer.State.began {
                let touchPoint = sender.location(in: collectionView)
                if let index = collectionView.indexPathForItem(at: touchPoint) {
                   
                    // Configure button handler
                    alert.addAction(UIAlertAction(title: delete, style: .destructive, handler: { _ in
                        
                        
                        let noteDelete = self.myNotes[index.row]
                        
                        self.context.delete(noteDelete)
                        
                        // Save the data
                        try? self.context.save()
                        
                        // Re-fetch the data
                        self.fetchNote()
                        
                        do{
                            try self.context.save()
                        }catch{
                            self.fetchNote()
                        }
                            }
                    ))
                    
                }
            }
        
       
        // Configure button handler
        alert.addAction(UIAlertAction(title: cancelAlert, style: .cancel, handler: { _ in
            }
        ))
        
        self.present(alert, animated: true, completion: nil)
    }

    let longPress = UILongPressGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
        notes.text = "Notes".localizedString()
        barBtnTakeNote.title = "TakeNote".localizedString()
        deleteDescription = "DeleteDescription".localizedString()
        
        // Fetch data
        self.fetchNote()
        
        self.view.addGestureRecognizer(longPress)
        longPress.addTarget(self, action: #selector(delete1(sender:)))
        
        // Reload Data in CollectionView(card)
        self.collectionView.reloadData()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        // Disable horizontal scroll bar in collectionView(card)
        collectionView.showsVerticalScrollIndicator = false
        
        // Register Nib
        collectionView.register(CardCollectionViewCell.nib(), forCellWithReuseIdentifier: CardCollectionViewCell.identifier)
        
    }

    // Create function Set Language
    func setLang(lang: String){
        AppService.shared.choose(language: lang)
        notes.text = "Notes".localizedString()
    }
    
    @IBAction func testLocalization(_ sender: UIBarButtonItem) {
        
        // Create alert
        let alert = UIAlertController(title: nil, message: descriptionAlert, preferredStyle: UIAlertController.Style.actionSheet)
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: khmerAlert, style: .default, handler: { _ in

                self.setLang(lang: language.khmer.rawValue)
            
                self.titleAlert = "Language".localizedString()
                self.cancelAlert = "Cancel".localizedString()
                self.descriptionAlert = "DescriptionAlert".localizedString()
                self.khmerAlert = "Khmer".localizedString()
                self.englishAlert = "English".localizedString()
                self.cancelAlert = "Cancel".localizedString()
                self.barBtnTakeNote.title = "TakeNote".localizedString()
                self.deleteDescription = "DeleteDescription".localizedString()
                self.delete = "Delete".localizedString()
                
                }))
        // Configure button handler
                alert.addAction(UIAlertAction(title: englishAlert, style: .default,
                handler: {(_: UIAlertAction!) in

                    self.setLang(lang: language.english.rawValue)
                    
                    self.titleAlert = "Language".localizedString()
                    self.cancelAlert = "Cancel".localizedString()
                    self.descriptionAlert = "DescriptionAlert".localizedString()
                    self.khmerAlert = "Khmer".localizedString()
                    self.englishAlert = "English".localizedString()
                    self.cancelAlert = "Cancel".localizedString()
                    self.barBtnTakeNote.title = "TakeNote".localizedString()
                    self.deleteDescription = "DeleteDescription".localizedString()
                    self.delete = "Delete".localizedString()
                }))
        
        // Configure button handler
        alert.addAction(UIAlertAction(title: cancelAlert, style: .cancel, handler: { _ in
                    
                }))
                self.present(alert, animated: true, completion: nil)
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    // Define Number of Items in a Section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return myNotes.count
    }
    
    // Display data in each part of cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell
    
        cell?.titleCard?.text = myNotes[indexPath.row].myTitle
        cell?.desCard?.text = myNotes[indexPath.row].myDescription

        return cell!
        
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func backToNote(titleEdit: String, descriptionEdit: String, indexNote: Int) {
        self.myNotes = try! context.fetch(Note.fetchRequest()) as! [Note]
        print(myNotes)
        let objNote = myNotes[indexNote]
        objNote.myTitle = titleEdit
        objNote.myDescription = descriptionEdit
       try? context.save()

    }
    
    
    // Get exact index of selected items
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        indexPathEdit = indexPath.row
        
        let editNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "noteId") as! NoteEditViewController
                
            self.delegate = editNoteViewController

        delegate?.leadToNote(titleEdit: myNotes[indexPath.row].myTitle ?? "", descriptionEdit: myNotes[indexPath.row].myDescription ?? "", indexNote: indexPathEdit ?? 0)

            self.navigationController?.pushViewController(editNoteViewController, animated: true)

        }
}

extension ViewController : UICollectionViewDelegateFlowLayout{
    
    // Set Size of Collection View Layout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 190, height: 280)
    }
    
    // Set minimum spacing for Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
    // Set minimum inter item spacing for Section
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5;
    }
    
}

extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: AppService.shared.language, ofType: "lproj")!
        let bundle = Bundle(path: path)!
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: self, comment: self)
    }
}

