
import UIKit

class CardCollectionViewCell: UICollectionViewCell{

    
   
  
    @IBOutlet weak var titleCard: UITextView!

    @IBOutlet weak var desCard: UILabel!
    
    //Create static identifier
    static var identifier = "cardCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }
        
    //Create static function of UINib
    static func nib() -> UINib {
        return UINib(nibName: "CardCollectionViewCell", bundle: nil)
    }
    
    //Create func to pass data from UINib to View Controller
    func configureStatus(title: String, description: String){
        self.titleCard?.text = title
        self.desCard?.text = description
    }

}
